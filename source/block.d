module omcdata.block;


enum Block : ubyte
{
    AIR = 0,
    STONE,
    GRASS,
    DIRT,
    COBBLE,
    PLANKS,
    SAPLING,
    BEDROCK,
    WATER_STILL,
    WATER_FLOW,

    LAVA_STILL,
    LAVA_FLOW,
    SAND,
    GRAVEL,
    ORE_GOLD,
    ORE_IRON,
    ORE_COAL,
    LOG,
    LEAVES,
    SPONGE,

    GLASS,
    ORE_LAPIS,
    BLOCK_LAPIS,
    DISPENSER,
    SANDSTONE,
    NOTEBLOCK,
    BED,
    RAIL_POWERED,
    RAIL_DETECTOR,
    PISTON_STICKY,

    COBWEB,
    TALL_GRASS,
    DEAD_SHRUB,
    PISTON,
    PISTON_HEAD,
    WOOL,
    UNKNOWN,
    FLOWER_YELLOW,
    FLOWER_RED,
    MUSHROOM_BROWN,

    MUSHROOM_RED,
    BLOCK_GOLD,
    BLOCK_IRON,
    DOUBLE_SLAB,
    SINGLE_SLAB,
    BRICK,
    TNT,
    BOOKSHELF,
    MOSSY_COBBLE,
    OBSIDIAN,

    TORCH,
    FIRE,
    MOB_SPAWNER,
    STAIR_WOOD,
    CHEST,
    REDSTONE_WIRE,
    ORE_DIAMOND,
    BLOCK_DIAMOND,
    CRAFTING_TABLE,
    CROP_WHEAT,

    FARMLAND,
    FURNACE_OFF,
    FURNACE_ON,
    SIGN_GROUND,
    DOOR_WOOD,
    LADDER,
    RAIL,
    STAIR_COBBLE,
    SIGN_WALL,
    LEVER,

    PRESSURE_PLATE_STONE,
    DOOR_IRON,
    PRESSURE_PLATE_WOOD,
    ORE_REDSTONE_OFF,
    ORE_REDSTONE_ON,
    REDSTONE_TORCH_OFF,
    REDSTONE_TORCH_ON,
    BUTTON_STONE,
    SNOW_LAYER,
    ICE,

    SNOW_BLOCK,
    CACTUS,
    CLAY,
    SUGARCANE,
    JUKEBOX,
    FENCE,
    PUMPKIN,
    NETHERRACK,
    SOUL_SAND,
    GLOWSTONE,

    PORTAL,
    JACK_O_LANTERN,
    CAKE,
    REPEATER_OFF,
    REPEATER_ON,
    CHEST_PHONY,
    TRAPDOOR,
}

enum Shape
{
    CUBE,
    CROSS,
    OTHER
}

struct BlockInfo
{
    string name;
    bool renderable;
    bool opaque;
    Shape shape;
}

static immutable BlockInfo[Block.max + 1] blockInfo = [
    Block.AIR:                  BlockInfo("Air",          false, false, Shape.OTHER ),
    Block.STONE:                BlockInfo("Stone",        true,  true,  Shape.CUBE  ),
    Block.GRASS:                BlockInfo("Grass",        true,  true,  Shape.CUBE  ),
    Block.DIRT:                 BlockInfo("Dirt",         true,  true,  Shape.CUBE  ),
    Block.COBBLE:               BlockInfo("Cobblestone",  true,  true,  Shape.CUBE  ),
    Block.PLANKS:               BlockInfo("Planks",       true,  true,  Shape.CUBE  ),
    Block.SAPLING:              BlockInfo("Sapling",      true,  false, Shape.CROSS ),
    Block.BEDROCK:              BlockInfo("Bedrock",      true,  true,  Shape.CUBE  ),
    Block.WATER_STILL:          BlockInfo(),
    Block.WATER_FLOW:           BlockInfo(),

    Block.LAVA_STILL:           BlockInfo(),
    Block.LAVA_FLOW:            BlockInfo(),
    Block.SAND:                 BlockInfo(),
    Block.GRAVEL:               BlockInfo(),
    Block.ORE_GOLD:             BlockInfo(),
    Block.ORE_IRON:             BlockInfo(),
    Block.ORE_COAL:             BlockInfo(),
    Block.LOG:                  BlockInfo(),
    Block.LEAVES:               BlockInfo(),
    Block.SPONGE:               BlockInfo(),

    Block.GLASS:                BlockInfo(),
    Block.ORE_LAPIS:            BlockInfo(),
    Block.BLOCK_LAPIS:          BlockInfo(),
    Block.DISPENSER:            BlockInfo(),
    Block.SANDSTONE:            BlockInfo(),
    Block.NOTEBLOCK:            BlockInfo(),
    Block.BED:                  BlockInfo(),
    Block.RAIL_POWERED:         BlockInfo(),
    Block.RAIL_DETECTOR:        BlockInfo(),
    Block.PISTON_STICKY:        BlockInfo(),

    Block.COBWEB:               BlockInfo(),
    Block.TALL_GRASS:           BlockInfo(),
    Block.DEAD_SHRUB:           BlockInfo(),
    Block.PISTON:               BlockInfo(),
    Block.PISTON_HEAD:          BlockInfo(),
    Block.WOOL:                 BlockInfo(),
    Block.UNKNOWN:              BlockInfo(),
    Block.FLOWER_YELLOW:        BlockInfo(),
    Block.FLOWER_RED:           BlockInfo(),
    Block.MUSHROOM_BROWN:       BlockInfo(),

    Block.MUSHROOM_RED:         BlockInfo(),
    Block.BLOCK_GOLD:           BlockInfo(),
    Block.BLOCK_IRON:           BlockInfo(),
    Block.DOUBLE_SLAB:          BlockInfo(),
    Block.SINGLE_SLAB:          BlockInfo(),
    Block.BRICK:                BlockInfo(),
    Block.TNT:                  BlockInfo(),
    Block.BOOKSHELF:            BlockInfo(),
    Block.MOSSY_COBBLE:         BlockInfo(),
    Block.OBSIDIAN:             BlockInfo(),

    Block.TORCH:                BlockInfo(),
    Block.FIRE:                 BlockInfo(),
    Block.MOB_SPAWNER:          BlockInfo(),
    Block.STAIR_WOOD:           BlockInfo(),
    Block.CHEST:                BlockInfo(),
    Block.REDSTONE_WIRE:        BlockInfo(),
    Block.ORE_DIAMOND:          BlockInfo(),
    Block.BLOCK_DIAMOND:        BlockInfo(),
    Block.CRAFTING_TABLE:       BlockInfo(),
    Block.CROP_WHEAT:           BlockInfo(),

    Block.FARMLAND:             BlockInfo(),
    Block.FURNACE_OFF:          BlockInfo(),
    Block.FURNACE_ON:           BlockInfo(),
    Block.SIGN_GROUND:          BlockInfo(),
    Block.DOOR_WOOD:            BlockInfo(),
    Block.LADDER:               BlockInfo(),
    Block.RAIL:                 BlockInfo(),
    Block.STAIR_COBBLE:         BlockInfo(),
    Block.SIGN_WALL:            BlockInfo(),
    Block.LEVER:                BlockInfo(),

    Block.PRESSURE_PLATE_STONE: BlockInfo(),
    Block.DOOR_IRON:            BlockInfo(),
    Block.PRESSURE_PLATE_WOOD:  BlockInfo(),
    Block.ORE_REDSTONE_OFF:     BlockInfo(),
    Block.ORE_REDSTONE_ON:      BlockInfo(),
    Block.REDSTONE_TORCH_OFF:   BlockInfo(),
    Block.REDSTONE_TORCH_ON:    BlockInfo(),
    Block.BUTTON_STONE:         BlockInfo(),
    Block.SNOW_LAYER:           BlockInfo(),
    Block.ICE:                  BlockInfo(),

    Block.SNOW_BLOCK:           BlockInfo(),
    Block.CACTUS:               BlockInfo(),
    Block.CLAY:                 BlockInfo(),
    Block.SUGARCANE:            BlockInfo(),
    Block.JUKEBOX:              BlockInfo(),
    Block.FENCE:                BlockInfo(),
    Block.PUMPKIN:              BlockInfo(),
    Block.NETHERRACK:           BlockInfo(),
    Block.SOUL_SAND:            BlockInfo(),
    Block.GLOWSTONE:            BlockInfo(),

    Block.PORTAL:               BlockInfo(),
    Block.JACK_O_LANTERN:       BlockInfo(),
    Block.CAKE:                 BlockInfo(),
    Block.REPEATER_OFF:         BlockInfo(),
    Block.REPEATER_ON:          BlockInfo(),
    Block.CHEST_PHONY:          BlockInfo(),
    Block.TRAPDOOR:             BlockInfo(),
];
